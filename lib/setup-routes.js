const Router = require('koa-better-router');
const koaBody = require('koa-body');

const controller = require('./controller');

module.exports = () => {
  let router = Router({ prefix: '/api' }).loadMethods();
  router.post('/auth', koaBody(), controller.createAuth);
  router.post('/auth/login', koaBody(), controller.login);
  router.post('/auth/logout', koaBody(), controller.logout);
  router.post('/auth/verifyToken', koaBody(), controller.verifyToken);
  router.post('/auth/signToken', koaBody(), controller.signToken);
  return router.middleware();
};
